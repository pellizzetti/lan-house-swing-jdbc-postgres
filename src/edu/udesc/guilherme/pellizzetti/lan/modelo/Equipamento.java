package edu.udesc.guilherme.pellizzetti.lan.modelo;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Equipamento {

    private Integer id;
    private String serial;
    //private Categoria categoria;
    private Integer categoria;
    private Double valor;

    private static final NumberFormat fmtNumero = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

    public Equipamento() {
    }

    public Equipamento(Integer id, String serial, Integer categoria, Double valor) {
        this.id = id;
        this.serial = serial;
        this.categoria = categoria;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    /*public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }*/
    
    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public static String convertValorToString(double valor) {
        return fmtNumero.format(valor);
    }

    public static double formatStringToValor(String strValor) throws ParseException {
        return fmtNumero.parse(strValor).doubleValue();
    }

    public String toString() {
        return this.categoria + ": " + this.serial;
    }

}
