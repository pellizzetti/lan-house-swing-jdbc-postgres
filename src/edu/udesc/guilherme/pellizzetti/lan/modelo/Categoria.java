package edu.udesc.guilherme.pellizzetti.lan.modelo;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Categoria {
    
    private Integer id;
    private String descricao;
    private Double valorHora;
    
    private static final NumberFormat fmtNumero = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
    
    public Categoria() {
    }

    public Categoria(Integer id, String descricao, Double valorHora) {
        this.id = id;
        this.descricao = descricao;
        this.valorHora = valorHora;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorHora() {
        return valorHora;
    }

    public void setValorHora(Double valorHora) {
        this.valorHora = valorHora;
    }
    
    public static String convertValorToString(double valor) {
        return fmtNumero.format(valor);
    }

    public static double formatStringToValor(String strValor) throws ParseException {
        return fmtNumero.parse(strValor).doubleValue();
    }
    
    public String toString() {
        return this.descricao;
    }
    
}
