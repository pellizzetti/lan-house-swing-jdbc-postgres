package edu.udesc.guilherme.pellizzetti.lan.modelo;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Horario {
    
    private Integer id;
    private Integer cliente;
    private Integer equipamento;
    private Double qntHoras;
    
    private static final NumberFormat fmtNumero = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
    
    public Horario() {
    }

    public Horario(Integer id, Integer cliente, Integer equipamento, Double valorHora) {
        this.id = id;
        this.cliente = cliente;
        this.equipamento = equipamento;
        this.qntHoras = valorHora;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }
    
    public Integer getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Integer equipamento) {
        this.equipamento = equipamento;
    }

    public Double getQntHoras() {
        return qntHoras;
    }

    public void setQntHoras(Double qntHoras) {
        this.qntHoras = qntHoras;
    }
    
    public static String convertQntHorasToString(double qntHoras) {
        return fmtNumero.format(qntHoras);
    }

    public static double formatStringToQntHoras(String strQntHoras) throws ParseException {
        return fmtNumero.parse(strQntHoras).doubleValue();
    }
    
    public String toString() {
        return "[ " + cliente + " - " + equipamento + " - " + qntHoras + " ]";
    }
    
}
