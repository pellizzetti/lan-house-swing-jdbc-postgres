package edu.udesc.guilherme.pellizzetti.lan.ex;

public class Excecao extends RuntimeException {

    public Excecao(Exception ex) {
        super(ex);
    }

    public Excecao(String mensagem) {
        super(mensagem);
    }

    public Excecao(String mensagem, Exception ex) {
        super(mensagem, ex);
    }

}
