package edu.udesc.guilherme.pellizzetti.lan.ui.pagamento;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class PagamentoFechaFrame extends JFrame {

    private JFormattedTextField txtId;
    private JFormattedTextField txtCliente;
     private JFormattedTextField txtEquipamento;
    private JTextField txtQntHora;

    private JButton btnFechar;
    private JButton bCancelar;

    private PagamentoFrame framePrincipal;

    public PagamentoFechaFrame(PagamentoFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Incluir horario");
        setSize(350, 300);
        setLocationRelativeTo(null);
        setResizable(false);
        inicializaComponentes();
        resetForm();
    }

    private void inicializaComponentes() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(montaPanelEditarHorario(), BorderLayout.CENTER);
        panel.add(montaPanelBotoesEditar(), BorderLayout.SOUTH);
        add(panel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    private JPanel montaPanelBotoesEditar() {
        JPanel panel = new JPanel();

        btnFechar = new JButton("Fechar");
        btnFechar.setMnemonic(KeyEvent.VK_S);
        btnFechar.addActionListener(new ExcluirHorarioListener());
        panel.add(btnFechar);

        bCancelar = new JButton("Cancelar");
        bCancelar.setMnemonic(KeyEvent.VK_C);
        bCancelar.addActionListener(new CancelarListener());
        panel.add(bCancelar);

        return panel;
    }

    private JPanel montaPanelEditarHorario() {
        JPanel painelEditarHorario = new JPanel();
        painelEditarHorario.setLayout(new GridLayout(8, 1));

        txtCliente = new JFormattedTextField();
        txtEquipamento = new JFormattedTextField();
        txtQntHora = new JTextField();
        txtId = new JFormattedTextField();
        txtId.setEnabled(false);
        
        painelEditarHorario.add(new JLabel("ID:"));
        painelEditarHorario.add(txtId);
        painelEditarHorario.add(new JLabel("Cliente (ID):"));
        painelEditarHorario.add(txtCliente);
        painelEditarHorario.add(new JLabel("Equipamento (ID):"));
        painelEditarHorario.add(txtEquipamento);
        painelEditarHorario.add(new JLabel("Qnt. Hora:"));
        painelEditarHorario.add(txtQntHora);

        return painelEditarHorario;
    }

    private void resetForm() {
        txtId.setValue(null);
        txtCliente.setValue(null);
        txtCliente.setValue(null);
        txtQntHora.setText("");
    }

    private void populaTextFields(Horario umaHorario) {
        txtId.setValue(umaHorario.getId());
        txtCliente.setValue(umaHorario.getCliente());
        txtEquipamento.setValue(umaHorario.getEquipamento());
        txtQntHora.setText(Horario.convertQntHorasToString(umaHorario.getQntHoras()));
    }

    protected Integer getIdHorario() {
        try {
            return Integer.parseInt(txtId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    private String validador() {
        StringBuilder sb = new StringBuilder();
        sb.append(txtCliente.getText() == null || "".equals(txtCliente.getText().trim()) ? "Cliente, " : "");
        sb.append(txtEquipamento.getText() == null || "".equals(txtEquipamento.getText().trim()) ? "Equipamento, " : "");
        sb.append(txtQntHora.getText() == null || "".equals(txtQntHora.getText().trim()) ? "Qnt. Horas, " : "");

        if (!sb.toString().isEmpty()) {
            sb.delete(sb.toString().length() - 2, sb.toString().length());
        }
        return sb.toString();
    }

    protected Horario carregaHorarios() {
        String msg = validador();
        if (!msg.isEmpty()) {
            throw new RuntimeException("Informe o(s) campo(s): " + msg);
        }

        Integer cliente = Integer.parseInt(txtCliente.getText().trim());
        Integer equipamento = Integer.parseInt(txtEquipamento.getText().trim());
        
        Double valorHora = null;
        try {
            valorHora = Horario.formatStringToQntHoras(txtQntHora.getText());
        } catch (ParseException nex) {
            throw new RuntimeException("Falha na conversão do campo Quantidade de horas (Double).\nConteudo inválido!");
        }
        if (valorHora < 1) {
            throw new RuntimeException("A quantidade mínima de horas deve ser 1");
        }

        return new Horario(null, cliente, equipamento, valorHora);
    }

    public void setHorario(Horario umaHorario) {
        resetForm();
        if (umaHorario != null) {
            populaTextFields(umaHorario);
        }
    }

    private class CancelarListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            resetForm();
        }
    }

    private class SalvarHorarioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                Horario m = carregaHorarios();
                HorarioDAO dao = new HorarioDBDAO();
                dao.grava(m);

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaHorariosAction());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(PagamentoFechaFrame.this,
                        ex.getMessage(), "Falha ao incluir o horario", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ExcluirHorarioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Integer id = getIdHorario();
            if (id != null) {
                try {
                    HorarioDAO dao = new HorarioDBDAO();
                    Horario umaHorario = dao.achaPorID(id);
                    if (umaHorario != null) {
                        dao.exclui(umaHorario);
                    }

                    setVisible(false);
                    resetForm();
                    SwingUtilities.invokeLater(framePrincipal.newConsultaHorariosAction());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(PagamentoFechaFrame.this,
                            ex.getMessage(), "Falha ao excluir o horario", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
