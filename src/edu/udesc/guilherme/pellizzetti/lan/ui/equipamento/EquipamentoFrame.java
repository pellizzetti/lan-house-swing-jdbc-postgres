package edu.udesc.guilherme.pellizzetti.lan.ui.equipamento;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;
import edu.udesc.guilherme.pellizzetti.lan.ui.PrincipalFrame;

public class EquipamentoFrame extends JFrame {

    private EquipamentoTable tabela;
    private JScrollPane scrollPane;
    private JButton btnNovoEquipamento;

    private EquipamentoInsereFrame incluirFrame;
    private EquipamentoEditaFrame editarFrame;
    private PrincipalFrame framePrincipal;

    public EquipamentoFrame(PrincipalFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Equipamentos");
        setSize(620,480);

        initComponents();
        adicionaComponentes();

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    private void initComponents() {
        tabela = new EquipamentoTable();
        tabela.addMouseListener(new EditarEquipamentoListener());
        scrollPane = new JScrollPane();
        scrollPane.setViewportView(tabela);

        btnNovoEquipamento = new JButton();
        btnNovoEquipamento.setText("Adicionar");
        btnNovoEquipamento.setMnemonic(KeyEvent.VK_N);
        btnNovoEquipamento.addActionListener(new IncluirEquipamentoListener());

        incluirFrame = new EquipamentoInsereFrame(this);
        editarFrame = new EquipamentoEditaFrame(this);

        initDB();
    }

    private void adicionaComponentes() {
        add(scrollPane);
        JPanel panel = new JPanel();
        panel.add(btnNovoEquipamento);
        add(panel, BorderLayout.SOUTH);
    }

    private void initDB() {
        try {
            new EquipamentoDBDAO().init();
            SwingUtilities.invokeLater(newConsultaEquipamentosAction());
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(this, "Falha ao inicializar o Banco de dados: "
                    + ex.getMessage() + "\nVerificar o driver ou configurações do banco", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    public Runnable newConsultaEquipamentosAction() {
        return new Runnable() {
            public void run() {
                try {
                    EquipamentoDAO dao = new EquipamentoDBDAO();
                    tabela.reload(dao.getTodos());
                } catch (Excecao ex) {
                    JOptionPane.showMessageDialog(EquipamentoFrame.this,
                            ex.getMessage(), "Falha ao consultar equipamento(s)", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    public void refreshTable(List<Equipamento> equipamentos) {
        tabela.reload(equipamentos);
    }

    private class IncluirEquipamentoListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            incluirFrame.setVisible(true);
        }
    }

    private class EditarEquipamentoListener extends MouseAdapter {

        public void mouseClicked(MouseEvent event) {
            if (event.getClickCount() == 2) {
                Equipamento umEquipamento = tabela.getEquipamentoSelecionado();
                if (umEquipamento != null) {
                    editarFrame.setEquipamento(umEquipamento);
                    editarFrame.setVisible(true);
                }
            }
        }
    }

}
