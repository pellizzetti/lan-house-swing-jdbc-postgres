package edu.udesc.guilherme.pellizzetti.lan.ui.equipamento;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class EquipamentoTableModel extends AbstractTableModel {

    private List<Equipamento> equipamentos;

    private String[] nomeColunas = {"Serial", "Categoria", "Valor"};
    private Class<?>[] tiposColunas = {String.class, Categoria.class, String.class};

    public EquipamentoTableModel() {
    }

    public void reload(List<Equipamento> equipamentos) {
        this.equipamentos = equipamentos;
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        return tiposColunas[coluna];
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int coluna) {
        return nomeColunas[coluna];
    }

    @Override
    public int getRowCount() {
        if (equipamentos == null) {
            return 0;
        }
        return equipamentos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Equipamento umEquipamento = equipamentos.get(linha);
        switch (coluna) {
            case 0:
                return umEquipamento.getSerial();
            case 1:
                return umEquipamento.getCategoria();
            case 2:
                return Equipamento.convertValorToString(umEquipamento.getValor());
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Equipamento getEquipamentoAt(int index) {
        return equipamentos.get(index);
    }
}
