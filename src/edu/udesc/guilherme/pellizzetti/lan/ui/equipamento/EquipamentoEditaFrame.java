package edu.udesc.guilherme.pellizzetti.lan.ui.equipamento;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;

public class EquipamentoEditaFrame extends EquipamentoInsereFrame {

	public EquipamentoEditaFrame(EquipamentoFrame framePrincipal) {
		super(framePrincipal);
		setTitle("Editar equipamento");
		bExcluir.setVisible(true);
	}
	
	protected Equipamento carregaEquipamentos() {
		Equipamento umEquipamento = super.carregaEquipamentos();
		umEquipamento.setId(getIdEquipamento());
		return umEquipamento;
	}
	
}
