package edu.udesc.guilherme.pellizzetti.lan.ui.equipamento;

import java.util.List;

import javax.swing.JTable;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;

public class EquipamentoTable extends JTable {

    private EquipamentoTableModel modelo;

    public EquipamentoTable() {
        modelo = new EquipamentoTableModel();
        setModel(modelo);
    }

    public Equipamento getEquipamentoSelecionado() {
        int i = getSelectedRow();
        if (i < 0) {
            return null;
        }
        return modelo.getEquipamentoAt(i);
    }

    public void reload(List<Equipamento> equipamentos) {
        modelo.reload(equipamentos);
    }
}
