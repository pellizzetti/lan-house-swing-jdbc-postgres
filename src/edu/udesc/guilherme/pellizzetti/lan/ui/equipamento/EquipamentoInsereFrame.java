package edu.udesc.guilherme.pellizzetti.lan.ui.equipamento;

import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDBDAO;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;

public class EquipamentoInsereFrame extends JFrame {

    private JFormattedTextField txtId;
    private JTextField txtSerial;
    //private JFormattedTextField txtCategoria;
    private JComboBox cbxCategoria;
    private JTextField txtValor;
    private ArrayList<Categoria> listaCategoria;

    private JButton bSalvar;
    private JButton bCancelar;
    protected JButton bExcluir;

    private EquipamentoFrame framePrincipal;

    public EquipamentoInsereFrame(EquipamentoFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Incluir equipamento");
        setSize(350, 300);
        setLocationRelativeTo(null);
        setResizable(false);
        inicializaComponentes();
        resetForm();
    }

    private void inicializaComponentes() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(montaPanelEditarEquipamento(), BorderLayout.CENTER);
        panel.add(montaPanelBotoesEditar(), BorderLayout.SOUTH);
        add(panel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    private JPanel montaPanelBotoesEditar() {
        JPanel panel = new JPanel();

        bSalvar = new JButton("Salvar");
        bSalvar.setMnemonic(KeyEvent.VK_S);
        bSalvar.addActionListener(new SalvarEquipamentoListener());
        panel.add(bSalvar);

        bCancelar = new JButton("Cancelar");
        bCancelar.setMnemonic(KeyEvent.VK_C);
        bCancelar.addActionListener(new CancelarListener());
        panel.add(bCancelar);

        bExcluir = new JButton();
        bExcluir.setText("Excluir");
        bExcluir.setMnemonic(KeyEvent.VK_E);
        bExcluir.addActionListener(new ExcluirEquipamentoListener());
        bExcluir.setVisible(false);
        panel.add(bExcluir);

        return panel;
    }

    private JPanel montaPanelEditarEquipamento() {
        JPanel painelEditarEquipamento = new JPanel();
        painelEditarEquipamento.setLayout(new GridLayout(8, 1));

        txtSerial = new JTextField();
        //txtCategoria = new JFormattedTextField();
        cbxCategoria = new JComboBox();
        txtValor = new JTextField();
        txtId = new JFormattedTextField();
        txtId.setEnabled(false);

        painelEditarEquipamento.add(new JLabel("ID:"));
        painelEditarEquipamento.add(txtId);
        painelEditarEquipamento.add(new JLabel("Serial:"));
        painelEditarEquipamento.add(txtSerial);
        painelEditarEquipamento.add(new JLabel("Categoria (ID):"));
        //painelEditarEquipamento.add(txtCategoria);
        painelEditarEquipamento.add(cbxCategoria);
        painelEditarEquipamento.add(new JLabel("Valor:"));
        painelEditarEquipamento.add(txtValor);

        return painelEditarEquipamento;
    }

    private void resetForm() {
        txtId.setValue(null);
        txtSerial.setText("");
        //txtCategoria.setValue(null);
        preencherCategoria();
        cbxCategoria.setSelectedIndex(0);
        txtValor.setText("");
    }

    private void populaCampos(Equipamento umEquipamento) {
        txtId.setValue(umEquipamento.getId());
        txtSerial.setText(umEquipamento.getSerial());
        //txtCategoria.setValue(umEquipamento.getCategoria());
        cbxCategoria.setSelectedIndex(0);
        txtValor.setText(Equipamento.convertValorToString(umEquipamento.getValor()));
    }

    protected Integer getIdEquipamento() {
        try {
            return Integer.parseInt(txtId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    private String validador() {
        StringBuilder sb = new StringBuilder();
        sb.append(txtSerial.getText() == null || "".equals(txtSerial.getText().trim()) ? "Serial, " : "");
        sb.append(txtValor.getText() == null || "".equals(txtValor.getText().trim()) ? "Valor, " : "");

        if (!sb.toString().isEmpty()) {
            sb.delete(sb.toString().length() - 2, sb.toString().length());
        }
        return sb.toString();
    }

    protected Equipamento carregaEquipamentos() {
        String msg = validador();
        if (!msg.isEmpty()) {
            throw new RuntimeException("Informe o(s) campo(s): " + msg);
        }

        String serial = txtSerial.getText().trim();
        Categoria categoria = (Categoria)cbxCategoria.getSelectedItem();
        Integer idCategoria = categoria.getId();

        if (serial.length() < 3) {
            throw new RuntimeException("O serial deve conter no mínimo 3 caracteres!");
        }

        Double valor = null;
        try {
            valor = Equipamento.formatStringToValor(txtValor.getText());
        } catch (ParseException nex) {
            throw new RuntimeException("Falha na conversão do campo valor (Double).\nConteudo inválido!");
        }
        if (valor < 1) {
            throw new RuntimeException("O valor mínimo do preço deve ser 1");
        }

        return new Equipamento(null, serial, idCategoria, valor);
    }

    public void setEquipamento(Equipamento umEquipamento) {
        resetForm();
        if (umEquipamento != null) {
            populaCampos(umEquipamento);
        }
    }

    private void preencherCategoria() {
        try {
            CategoriaDAO dao = new CategoriaDBDAO();
            listaCategoria = (ArrayList<Categoria>) dao.getTodos();
            cbxCategoria.removeAllItems();
            for (Categoria categoria : listaCategoria) {
                cbxCategoria.addItem(categoria);
            }
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(EquipamentoInsereFrame.this,
                    ex.getMessage(), "Falha ao preencher categoria", JOptionPane.ERROR_MESSAGE);
        }
    }

    private class CancelarListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            resetForm();
        }
    }

    private class SalvarEquipamentoListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                Equipamento umEquipamento = carregaEquipamentos();
                EquipamentoDAO dao = new EquipamentoDBDAO();
                dao.grava(umEquipamento);

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaEquipamentosAction());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(EquipamentoInsereFrame.this,
                        ex.getMessage(), "Falha ao incluir o equipamento", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ExcluirEquipamentoListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        Integer id = getIdEquipamento();
        if (id != null) {
            try {
                EquipamentoDAO dao = new EquipamentoDBDAO();
                Equipamento m = dao.achaPorID(id);
                if (m != null) {
                    dao.exclui(m);
                }

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaEquipamentosAction());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(EquipamentoInsereFrame.this,
                        ex.getMessage(), "Falha ao excluir o equipamento", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
}
