package edu.udesc.guilherme.pellizzetti.lan.ui.cliente;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public class ClienteInsereFrame extends JFrame {

    private JFormattedTextField txtId;
    private JTextField txtNome;
    private JFormattedTextField txtCpf;
    private JTextField txtTelefone;

    private JButton bSalvar;
    private JButton bCancelar;
    protected JButton bExcluir;

    private ClienteFrame framePrincipal;

    public ClienteInsereFrame(ClienteFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Incluir cliente");
        setSize(350, 300);
        setLocationRelativeTo(null);
        setResizable(false);
        inicializaComponentes();
        resetForm();
    }

    private void inicializaComponentes() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(montaPanelEditarCliente(), BorderLayout.CENTER);
        panel.add(montaPanelBotoesEditar(), BorderLayout.SOUTH);
        add(panel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    private JPanel montaPanelBotoesEditar() {
        JPanel panel = new JPanel();

        bSalvar = new JButton("Salvar");
        bSalvar.setMnemonic(KeyEvent.VK_S);
        bSalvar.addActionListener(new SalvarClienteListener());
        panel.add(bSalvar);

        bCancelar = new JButton("Cancelar");
        bCancelar.setMnemonic(KeyEvent.VK_C);
        bCancelar.addActionListener(new CancelarListener());
        panel.add(bCancelar);

        bExcluir = new JButton();
        bExcluir.setText("Excluir");
        bExcluir.setMnemonic(KeyEvent.VK_E);
        bExcluir.addActionListener(new ExcluirClienteListener());
        bExcluir.setVisible(false);
        panel.add(bExcluir);

        return panel;
    }

    private JPanel montaPanelEditarCliente() {
        JPanel painelEditarCliente = new JPanel();
        painelEditarCliente.setLayout(new GridLayout(8, 1));

        txtNome = new JTextField();
        txtCpf = new JFormattedTextField();
        txtTelefone = new JTextField();
        txtId = new JFormattedTextField();
        txtId.setEnabled(false);
        
        painelEditarCliente.add(new JLabel("ID:"));
        painelEditarCliente.add(txtId);
        painelEditarCliente.add(new JLabel("Nome:"));
        painelEditarCliente.add(txtNome);
        painelEditarCliente.add(new JLabel("CPF:"));
        painelEditarCliente.add(txtCpf);
        painelEditarCliente.add(new JLabel("Telefone:"));
        painelEditarCliente.add(txtTelefone);

        return painelEditarCliente;
    }

    private void resetForm() {
        txtId.setValue(null);
        txtNome.setText("");
        txtCpf.setValue("");
        txtTelefone.setText("");
    }

    private void populaTextFields(Cliente umCliente) {
        txtId.setValue(umCliente.getId());
        txtNome.setText(umCliente.getNome());
        txtCpf.setText(umCliente.getCpf());
        txtTelefone.setText(umCliente.getTelefone());
    }

    protected Integer getIdCliente() {
        try {
            return Integer.parseInt(txtId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    private String validador() {
        StringBuilder sb = new StringBuilder();
        sb.append(txtNome.getText() == null || "".equals(txtNome.getText().trim()) ? "Nome, " : "");
        sb.append(txtCpf.getText() == null || "".equals(txtCpf.getText().trim()) ? "CPF, " : "");
        sb.append(txtTelefone.getText() == null || "".equals(txtTelefone.getText().trim()) ? "Telefone, " : "");

        if (!sb.toString().isEmpty()) {
            sb.delete(sb.toString().length() - 2, sb.toString().length());
        }
        return sb.toString();
    }

    protected Cliente carregaClientes() {
        String msg = validador();
        if (!msg.isEmpty()) {
            throw new RuntimeException("Informe o(s) campo(s): " + msg);
        }

        String nome = txtNome.getText().trim();
        String cpf = txtCpf.getText().trim();
        String telefone = txtTelefone.getText().trim();

        if (nome.length() < 3) {
            throw new RuntimeException("O nome deve conter no mínimo 3 caracteres!");
        }

        return new Cliente(null, nome, cpf, telefone);
    }

    public void setCliente(Cliente umCliente) {
        resetForm();
        if (umCliente != null) {
            populaTextFields(umCliente);
        }
    }

    private class CancelarListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            resetForm();
        }
    }

    private class SalvarClienteListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                Cliente m = carregaClientes();
                ClienteDAO dao = new ClienteDBDAO();
                dao.grava(m);

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaClientesAction());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ClienteInsereFrame.this,
                        ex.getMessage(), "Falha ao incluir o cliente", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ExcluirClienteListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Integer id = getIdCliente();
            if (id != null) {
                try {
                    ClienteDAO dao = new ClienteDBDAO();
                    Cliente m = dao.achaPorID(id);
                    if (m != null) {
                        dao.exclui(m);
                    }

                    setVisible(false);
                    resetForm();
                    SwingUtilities.invokeLater(framePrincipal.newConsultaClientesAction());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(ClienteInsereFrame.this,
                            ex.getMessage(), "Falha ao excluir o cliente", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
