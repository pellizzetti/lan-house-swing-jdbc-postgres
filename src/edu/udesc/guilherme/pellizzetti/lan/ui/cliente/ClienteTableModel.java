package edu.udesc.guilherme.pellizzetti.lan.ui.cliente;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public class ClienteTableModel extends AbstractTableModel {

    private List<Cliente> clientes;

    private String[] nomeColunas = {"Nome", "CPF", "Telefone"};
    private Class<?>[] tiposColunas = {String.class, String.class, String.class};

    public ClienteTableModel() {
    }

    public void reload(List<Cliente> mercadorias) {
        this.clientes = mercadorias;
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        return tiposColunas[coluna];
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int coluna) {
        return nomeColunas[coluna];
    }

    @Override
    public int getRowCount() {
        if (clientes == null) {
            return 0;
        }
        return clientes.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Cliente umCliente = clientes.get(linha);
        switch (coluna) {
            case 0:
                return umCliente.getNome();
            case 1:
                return umCliente.getCpf();
            case 2:
                return umCliente.getTelefone();
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Cliente getClienteAt(int index) {
        return clientes.get(index);
    }
}
