package edu.udesc.guilherme.pellizzetti.lan.ui.cliente;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public class ClienteEditaFrame extends ClienteInsereFrame {

	public ClienteEditaFrame(ClienteFrame framePrincipal) {
		super(framePrincipal);
		setTitle("Editar cliente");
		bExcluir.setVisible(true);
	}
	
	protected Cliente carregaClientes() {
		Cliente umCliente = super.carregaClientes();
		umCliente.setId(getIdCliente());
		return umCliente;
	}
	
}
