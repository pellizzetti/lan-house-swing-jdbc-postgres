package edu.udesc.guilherme.pellizzetti.lan.ui.cliente;

import java.util.List;

import javax.swing.JTable;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public class ClienteTable extends JTable {

    private ClienteTableModel modelo;

    public ClienteTable() {
        modelo = new ClienteTableModel();
        setModel(modelo);
    }

    public Cliente getClienteSelecionado() {
        int i = getSelectedRow();
        if (i < 0) {
            return null;
        }
        return modelo.getClienteAt(i);
    }

    public void reload(List<Cliente> clientes) {
        modelo.reload(clientes);
    }
}
