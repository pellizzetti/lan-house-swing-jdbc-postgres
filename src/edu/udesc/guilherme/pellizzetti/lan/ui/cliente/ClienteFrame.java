package edu.udesc.guilherme.pellizzetti.lan.ui.cliente;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;
import edu.udesc.guilherme.pellizzetti.lan.ui.PrincipalFrame;

public class ClienteFrame extends JFrame {

    private ClienteTable tabela;
    private JScrollPane scrollPane;
    private JButton btnNovoCliente;

    private ClienteInsereFrame incluirFrame;
    private ClienteEditaFrame editarFrame;
    private PrincipalFrame framePrincipal;

    public ClienteFrame(PrincipalFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Clientes");
        setSize(620,480);

        initComponents();
        adicionaComponentes();

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    private void initComponents() {
        tabela = new ClienteTable();
        tabela.addMouseListener(new EditarClienteListener());
        scrollPane = new JScrollPane();
        scrollPane.setViewportView(tabela);

        btnNovoCliente = new JButton();
        btnNovoCliente.setText("Adicionar");
        btnNovoCliente.setMnemonic(KeyEvent.VK_N);
        btnNovoCliente.addActionListener(new IncluirClienteListener());

        incluirFrame = new ClienteInsereFrame(this);
        editarFrame = new ClienteEditaFrame(this);

        initDB();
    }

    private void adicionaComponentes() {
        add(scrollPane);
        JPanel panel = new JPanel();
        panel.add(btnNovoCliente);
        add(panel, BorderLayout.SOUTH);
    }

    private void initDB() {
        try {
            new ClienteDBDAO().init();
            SwingUtilities.invokeLater(newConsultaClientesAction());
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(this, "Falha ao inicializar o Banco de dados: "
                    + ex.getMessage() + "\nVerificar o driver ou configurações do banco", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    public Runnable newConsultaClientesAction() {
        return new Runnable() {
            public void run() {
                try {
                    ClienteDAO dao = new ClienteDBDAO();
                    tabela.reload(dao.getTodos());
                } catch (Excecao ex) {
                    JOptionPane.showMessageDialog(ClienteFrame.this,
                            ex.getMessage(), "Falha ao consultar cliente(s)", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    public void refreshTable(List<Cliente> clientes) {
        tabela.reload(clientes);
    }

    private class IncluirClienteListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            incluirFrame.setVisible(true);
        }
    }

    private class EditarClienteListener extends MouseAdapter {

        public void mouseClicked(MouseEvent event) {
            if (event.getClickCount() == 2) {
                Cliente umCliente = tabela.getClienteSelecionado();
                if (umCliente != null) {
                    editarFrame.setCliente(umCliente);
                    editarFrame.setVisible(true);
                }
            }
        }
    }

}
