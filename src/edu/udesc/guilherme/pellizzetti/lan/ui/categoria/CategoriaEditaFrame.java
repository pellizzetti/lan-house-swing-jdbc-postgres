package edu.udesc.guilherme.pellizzetti.lan.ui.categoria;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class CategoriaEditaFrame extends CategoriaInsereFrame {

	public CategoriaEditaFrame(CategoriaFrame framePrincipal) {
		super(framePrincipal);
		setTitle("Editar categoria");
		bExcluir.setVisible(true);
	}
	
	protected Categoria carregaCategorias() {
		Categoria umCategoria = super.carregaCategorias();
		umCategoria.setId(getIdCategoria());
		return umCategoria;
	}
	
}
