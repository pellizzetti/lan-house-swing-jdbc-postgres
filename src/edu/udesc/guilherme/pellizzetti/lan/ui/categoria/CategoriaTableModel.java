package edu.udesc.guilherme.pellizzetti.lan.ui.categoria;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class CategoriaTableModel extends AbstractTableModel {

    private List<Categoria> categorias;

    private String[] nomeColunas = {"Descrição", "Valor/Hora"};
    private Class<?>[] tiposColunas = {String.class, String.class};

    public CategoriaTableModel() {
    }

    public void reload(List<Categoria> categorias) {
        this.categorias = categorias;
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        return tiposColunas[coluna];
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int coluna) {
        return nomeColunas[coluna];
    }

    @Override
    public int getRowCount() {
        if (categorias == null) {
            return 0;
        }
        return categorias.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Categoria umCategoria = categorias.get(linha);
        switch (coluna) {
            case 0:
                return umCategoria.getDescricao();
            case 1:
                return umCategoria.convertValorToString(umCategoria.getValorHora());
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Categoria getCategoriaAt(int index) {
        return categorias.get(index);
    }
}
