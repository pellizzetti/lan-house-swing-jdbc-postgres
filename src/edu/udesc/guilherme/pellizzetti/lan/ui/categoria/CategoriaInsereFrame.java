package edu.udesc.guilherme.pellizzetti.lan.ui.categoria;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class CategoriaInsereFrame extends JFrame {

    private JFormattedTextField txtId;
    private JTextField txtDescricao;
    private JTextField txtValorHora;

    private JButton bSalvar;
    private JButton bCancelar;
    protected JButton bExcluir;

    private CategoriaFrame framePrincipal;

    public CategoriaInsereFrame(CategoriaFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Incluir categoria");
        setSize(350, 300);
        setLocationRelativeTo(null);
        setResizable(false);
        inicializaComponentes();
        resetForm();
    }

    private void inicializaComponentes() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(montaPanelEditarCategoria(), BorderLayout.CENTER);
        panel.add(montaPanelBotoesEditar(), BorderLayout.SOUTH);
        add(panel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    private JPanel montaPanelBotoesEditar() {
        JPanel panel = new JPanel();

        bSalvar = new JButton("Salvar");
        bSalvar.setMnemonic(KeyEvent.VK_S);
        bSalvar.addActionListener(new SalvarCategoriaListener());
        panel.add(bSalvar);

        bCancelar = new JButton("Cancelar");
        bCancelar.setMnemonic(KeyEvent.VK_C);
        bCancelar.addActionListener(new CancelarListener());
        panel.add(bCancelar);

        bExcluir = new JButton();
        bExcluir.setText("Excluir");
        bExcluir.setMnemonic(KeyEvent.VK_E);
        bExcluir.addActionListener(new ExcluirCategoriaListener());
        bExcluir.setVisible(false);
        panel.add(bExcluir);

        return panel;
    }

    private JPanel montaPanelEditarCategoria() {
        JPanel painelEditarCategoria = new JPanel();
        painelEditarCategoria.setLayout(new GridLayout(8, 1));

        txtDescricao = new JTextField();
        txtValorHora = new JTextField();
        txtId = new JFormattedTextField();
        txtId.setEnabled(false);
        
        painelEditarCategoria.add(new JLabel("ID:"));
        painelEditarCategoria.add(txtId);
        painelEditarCategoria.add(new JLabel("Descrição:"));
        painelEditarCategoria.add(txtDescricao);
        painelEditarCategoria.add(new JLabel("Valor/Hora:"));
        painelEditarCategoria.add(txtValorHora);

        return painelEditarCategoria;
    }

    private void resetForm() {
        txtId.setValue(null);
        txtDescricao.setText("");
        txtValorHora.setText("");
    }

    private void populaTextFields(Categoria umaCategoria) {
        txtId.setValue(umaCategoria.getId());
        txtDescricao.setText(umaCategoria.getDescricao());
        txtValorHora.setText(Categoria.convertValorToString(umaCategoria.getValorHora()));
    }

    protected Integer getIdCategoria() {
        try {
            return Integer.parseInt(txtId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    private String validador() {
        StringBuilder sb = new StringBuilder();
        sb.append(txtDescricao.getText() == null || "".equals(txtDescricao.getText().trim()) ? "Descrição, " : "");
        sb.append(txtValorHora.getText() == null || "".equals(txtValorHora.getText().trim()) ? "Valor/Hora, " : "");

        if (!sb.toString().isEmpty()) {
            sb.delete(sb.toString().length() - 2, sb.toString().length());
        }
        return sb.toString();
    }

    protected Categoria carregaCategorias() {
        String msg = validador();
        if (!msg.isEmpty()) {
            throw new RuntimeException("Informe o(s) campo(s): " + msg);
        }

        String descricao = txtDescricao.getText().trim();

        if (descricao.length() < 3) {
            throw new RuntimeException("O descricao deve conter no mínimo 3 caracteres!");
        }
        
        Double valorHora = null;
        try {
            valorHora = Categoria.formatStringToValor(txtValorHora.getText());
        } catch (ParseException nex) {
            throw new RuntimeException("Falha na conversão do campo valorHora (Double).\nConteudo inválido!");
        }
        if (valorHora < 1) {
            throw new RuntimeException("O valorHora mínimo do preço deve ser 1");
        }

        return new Categoria(null, descricao, valorHora);
    }

    public void setCategoria(Categoria umaCategoria) {
        resetForm();
        if (umaCategoria != null) {
            populaTextFields(umaCategoria);
        }
    }

    private class CancelarListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            resetForm();
        }
    }

    private class SalvarCategoriaListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                Categoria m = carregaCategorias();
                CategoriaDAO dao = new CategoriaDBDAO();
                dao.grava(m);

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaCategoriasAction());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(CategoriaInsereFrame.this,
                        ex.getMessage(), "Falha ao incluir o categoria", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ExcluirCategoriaListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Integer id = getIdCategoria();
            if (id != null) {
                try {
                    CategoriaDAO dao = new CategoriaDBDAO();
                    Categoria umaCategoria = dao.achaPorID(id);
                    if (umaCategoria != null) {
                        dao.exclui(umaCategoria);
                    }

                    setVisible(false);
                    resetForm();
                    SwingUtilities.invokeLater(framePrincipal.newConsultaCategoriasAction());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CategoriaInsereFrame.this,
                            ex.getMessage(), "Falha ao excluir o categoria", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
