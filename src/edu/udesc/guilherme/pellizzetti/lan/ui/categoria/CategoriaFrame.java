package edu.udesc.guilherme.pellizzetti.lan.ui.categoria;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.categoria.CategoriaDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;
import edu.udesc.guilherme.pellizzetti.lan.ui.PrincipalFrame;

public class CategoriaFrame extends JFrame {

    private CategoriaTable tabela;
    private JScrollPane scrollPane;
    private JButton btnNovoCategoria;

    private CategoriaInsereFrame incluirFrame;
    private CategoriaEditaFrame editarFrame;
    private PrincipalFrame framePrincipal;

    public CategoriaFrame(PrincipalFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Categorias");
        setSize(620,480);

        initComponents();
        adicionaComponentes();

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    private void initComponents() {
        tabela = new CategoriaTable();
        tabela.addMouseListener(new EditarCategoriaListener());
        scrollPane = new JScrollPane();
        scrollPane.setViewportView(tabela);

        btnNovoCategoria = new JButton();
        btnNovoCategoria.setText("Adicionar");
        btnNovoCategoria.setMnemonic(KeyEvent.VK_N);
        btnNovoCategoria.addActionListener(new IncluirCategoriaListener());

        incluirFrame = new CategoriaInsereFrame(this);
        editarFrame = new CategoriaEditaFrame(this);

        initDB();
    }

    private void adicionaComponentes() {
        add(scrollPane);
        JPanel panel = new JPanel();
        panel.add(btnNovoCategoria);
        add(panel, BorderLayout.SOUTH);
    }

    private void initDB() {
        try {
            new CategoriaDBDAO().init();
            SwingUtilities.invokeLater(newConsultaCategoriasAction());
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(this, "Falha ao inicializar o Banco de dados: "
                    + ex.getMessage() + "\nVerificar o driver ou configurações do banco", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    public Runnable newConsultaCategoriasAction() {
        return new Runnable() {
            public void run() {
                try {
                    CategoriaDAO dao = new CategoriaDBDAO();
                    tabela.reload(dao.getTodos());
                } catch (Excecao ex) {
                    JOptionPane.showMessageDialog(CategoriaFrame.this,
                            ex.getMessage(), "Falha ao consultar categoria(s)", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    public void refreshTable(List<Categoria> categorias) {
        tabela.reload(categorias);
    }

    private class IncluirCategoriaListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            incluirFrame.setVisible(true);
        }
    }

    private class EditarCategoriaListener extends MouseAdapter {

        public void mouseClicked(MouseEvent event) {
            if (event.getClickCount() == 2) {
                Categoria umCategoria = tabela.getCategoriaSelecionado();
                if (umCategoria != null) {
                    editarFrame.setCategoria(umCategoria);
                    editarFrame.setVisible(true);
                }
            }
        }
    }

}
