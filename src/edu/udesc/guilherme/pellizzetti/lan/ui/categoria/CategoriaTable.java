package edu.udesc.guilherme.pellizzetti.lan.ui.categoria;

import java.util.List;

import javax.swing.JTable;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class CategoriaTable extends JTable {

    private CategoriaTableModel modelo;

    public CategoriaTable() {
        modelo = new CategoriaTableModel();
        setModel(modelo);
    }

    public Categoria getCategoriaSelecionado() {
        int i = getSelectedRow();
        if (i < 0) {
            return null;
        }
        return modelo.getCategoriaAt(i);
    }

    public void reload(List<Categoria> categorias) {
        modelo.reload(categorias);
    }
}
