package edu.udesc.guilherme.pellizzetti.lan.ui.horario;

import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.cliente.ClienteDBDAO;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.equipamento.EquipamentoDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class HorarioInsereFrame extends JFrame {

    private JFormattedTextField txtId;
    //private JFormattedTextField cbxCliente;
    //private JFormattedTextField cbxEquipamento;
    private JComboBox cbxCliente;
    private JComboBox cbxEquipamento;
    private JTextField txtQntHora;
    private ArrayList<Equipamento> listaEquipamento;
    private ArrayList<Cliente> listaCliente;

    private JButton bSalvar;
    private JButton bCancelar;
    protected JButton bExcluir;

    private HorarioFrame framePrincipal;

    public HorarioInsereFrame(HorarioFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Incluir horario");
        setSize(350, 300);
        setLocationRelativeTo(null);
        setResizable(false);
        inicializaComponentes();
        resetForm();
    }

    private void inicializaComponentes() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(montaPanelEditarHorario(), BorderLayout.CENTER);
        panel.add(montaPanelBotoesEditar(), BorderLayout.SOUTH);
        add(panel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
    }

    private JPanel montaPanelBotoesEditar() {
        JPanel panel = new JPanel();

        bSalvar = new JButton("Salvar");
        bSalvar.setMnemonic(KeyEvent.VK_S);
        bSalvar.addActionListener(new SalvarHorarioListener());
        panel.add(bSalvar);

        bCancelar = new JButton("Cancelar");
        bCancelar.setMnemonic(KeyEvent.VK_C);
        bCancelar.addActionListener(new CancelarListener());
        panel.add(bCancelar);

        bExcluir = new JButton();
        bExcluir.setText("Excluir");
        bExcluir.setMnemonic(KeyEvent.VK_E);
        bExcluir.addActionListener(new ExcluirHorarioListener());
        bExcluir.setVisible(false);
        panel.add(bExcluir);

        return panel;
    }

    private JPanel montaPanelEditarHorario() {
        JPanel painelEditarHorario = new JPanel();
        painelEditarHorario.setLayout(new GridLayout(8, 1));

        cbxCliente = new JComboBox();
        cbxEquipamento = new JComboBox();
        txtQntHora = new JTextField();
        txtId = new JFormattedTextField();
        txtId.setEnabled(false);
        
        painelEditarHorario.add(new JLabel("ID:"));
        painelEditarHorario.add(txtId);
        painelEditarHorario.add(new JLabel("Cliente (ID):"));
        //painelEditarHorario.add(cbxCliente);
        painelEditarHorario.add(new JLabel("Equipamento (ID):"));
        //painelEditarHorario.add(cbxEquipamento);
        painelEditarHorario.add(new JLabel("Qnt. Hora:"));
        painelEditarHorario.add(txtQntHora);

        return painelEditarHorario;
    }

    private void resetForm() {
        txtId.setValue(null);
        //txtCliente.setValue(null);
        //txtEquipamento.setValue(null);
        //preencherCliente();
        //preencherEquipamento();
        cbxCliente.setSelectedIndex(0);
        cbxEquipamento.setSelectedIndex(0);
        txtQntHora.setText("");
    }

    private void populaTextFields(Horario umaHorario) {
        txtId.setValue(umaHorario.getId());
        //cbxCliente.setValue(umaHorario.getCliente());
        //cbxEquipamento.setValue(umaHorario.getEquipamento());
        cbxCliente.setSelectedIndex(0);
        cbxEquipamento.setSelectedIndex(0);
        txtQntHora.setText(Horario.convertQntHorasToString(umaHorario.getQntHoras()));
    }

    protected Integer getIdHorario() {
        try {
            return Integer.parseInt(txtId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    private String validador() {
        StringBuilder sb = new StringBuilder();
        sb.append(txtQntHora.getText() == null || "".equals(txtQntHora.getText().trim()) ? "Qnt. Horas, " : "");

        if (!sb.toString().isEmpty()) {
            sb.delete(sb.toString().length() - 2, sb.toString().length());
        }
        return sb.toString();
    }

    protected Horario carregaHorarios() {
        String msg = validador();
        if (!msg.isEmpty()) {
            throw new RuntimeException("Informe o(s) campo(s): " + msg);
        }

        Cliente cliente = (Cliente)cbxCliente.getSelectedItem();
        Integer idCliente = cliente.getId();
        Equipamento equipamento = (Equipamento)cbxEquipamento.getSelectedItem();
        Integer idEquipamento = equipamento.getId();
        
        Double valorHora = null;
        try {
            valorHora = Horario.formatStringToQntHoras(txtQntHora.getText());
        } catch (ParseException nex) {
            throw new RuntimeException("Falha na conversão do campo Quantidade de horas (Double).\nConteudo inválido!");
        }
        if (valorHora < 1) {
            throw new RuntimeException("A quantidade mínima de horas deve ser 1");
        }

        return new Horario(null, idCliente, idEquipamento, valorHora);
    }

    public void setHorario(Horario umaHorario) {
        resetForm();
        if (umaHorario != null) {
            populaTextFields(umaHorario);
        }
    }
    
    private void preencherCliente() {
        try {
            ClienteDAO dao = new ClienteDBDAO();
            listaCliente = (ArrayList<Cliente>) dao.getTodos();
            cbxCliente.removeAllItems();
            for (Cliente cliente : listaCliente) {
                cbxCliente.addItem(cliente);
            }
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(HorarioInsereFrame.this,
                    ex.getMessage(), "Falha ao preencher histori", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void preencherEquipamento() {
        try {
            EquipamentoDAO dao = new EquipamentoDBDAO();
            listaEquipamento = (ArrayList<Equipamento>) dao.getTodos();
            cbxEquipamento.removeAllItems();
            for (Equipamento equipamento : listaEquipamento) {
                cbxEquipamento.addItem(equipamento);
            }
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(HorarioInsereFrame.this,
                    ex.getMessage(), "Falha ao preencher histori", JOptionPane.ERROR_MESSAGE);
        }
    }

    private class CancelarListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            resetForm();
        }
    }

    private class SalvarHorarioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                Horario m = carregaHorarios();
                HorarioDAO dao = new HorarioDBDAO();
                dao.grava(m);

                setVisible(false);
                resetForm();
                SwingUtilities.invokeLater(framePrincipal.newConsultaHorariosAction());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(HorarioInsereFrame.this,
                        ex.getMessage(), "Falha ao incluir o horario", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ExcluirHorarioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Integer id = getIdHorario();
            if (id != null) {
                try {
                    HorarioDAO dao = new HorarioDBDAO();
                    Horario umaHorario = dao.achaPorID(id);
                    if (umaHorario != null) {
                        dao.exclui(umaHorario);
                    }

                    setVisible(false);
                    resetForm();
                    SwingUtilities.invokeLater(framePrincipal.newConsultaHorariosAction());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(HorarioInsereFrame.this,
                            ex.getMessage(), "Falha ao excluir o horario", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
