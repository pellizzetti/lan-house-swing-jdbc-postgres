package edu.udesc.guilherme.pellizzetti.lan.ui.horario;

import java.util.List;

import javax.swing.JTable;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class HorarioTable extends JTable {

    private HorarioTableModel modelo;

    public HorarioTable() {
        modelo = new HorarioTableModel();
        setModel(modelo);
    }

    public Horario getHorarioSelecionado() {
        int i = getSelectedRow();
        if (i < 0) {
            return null;
        }
        return modelo.getHorarioAt(i);
    }

    public void reload(List<Horario> horarios) {
        modelo.reload(horarios);
    }
}
