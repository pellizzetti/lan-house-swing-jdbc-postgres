package edu.udesc.guilherme.pellizzetti.lan.ui.horario;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class HorarioEditaFrame extends HorarioInsereFrame {

	public HorarioEditaFrame(HorarioFrame framePrincipal) {
		super(framePrincipal);
		setTitle("Editar horario");
		bExcluir.setVisible(true);
	}
	
	protected Horario carregaHorarios() {
		Horario umHorario = super.carregaHorarios();
		umHorario.setId(getIdHorario());
		return umHorario;
	}
	
}
