package edu.udesc.guilherme.pellizzetti.lan.ui.horario;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class HorarioTableModel extends AbstractTableModel {

    private List<Horario> horarios;

    private String[] nomeColunas = {"Cliente", "Equipamento", "Qnt. Horas"};
    private Class<?>[] tiposColunas = {Integer.class, Integer.class, String.class};

    public HorarioTableModel() {
    }

    public void reload(List<Horario> mercadorias) {
        this.horarios = mercadorias;
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        return tiposColunas[coluna];
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int coluna) {
        return nomeColunas[coluna];
    }

    @Override
    public int getRowCount() {
        if (horarios == null) {
            return 0;
        }
        return horarios.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Horario umHorario = horarios.get(linha);
        switch (coluna) {
            case 0:
                return umHorario.getCliente();
            case 1:
                return umHorario.getEquipamento();
            case 2:
                return Horario.convertQntHorasToString(umHorario.getQntHoras());
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Horario getHorarioAt(int index) {
        return horarios.get(index);
    }
}
