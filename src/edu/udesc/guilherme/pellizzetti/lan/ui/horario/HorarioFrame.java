package edu.udesc.guilherme.pellizzetti.lan.ui.horario;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDAO;
import edu.udesc.guilherme.pellizzetti.lan.dao.horario.HorarioDBDAO;
import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;
import edu.udesc.guilherme.pellizzetti.lan.ui.PrincipalFrame;

public class HorarioFrame extends JFrame {

    private HorarioTable tabela;
    private JScrollPane scrollPane;
    private JButton btnNovoHorario;

    private HorarioInsereFrame incluirFrame;
    private HorarioEditaFrame editarFrame;
    private PrincipalFrame framePrincipal;

    public HorarioFrame(PrincipalFrame framePrincipal) {
        this.framePrincipal = framePrincipal;
        setTitle("Horarios");
        setSize(620,480);

        initComponents();
        adicionaComponentes();

        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(false);
    }

    private void initComponents() {
        tabela = new HorarioTable();
        tabela.addMouseListener(new EditarHorarioListener());
        scrollPane = new JScrollPane();
        scrollPane.setViewportView(tabela);

        btnNovoHorario = new JButton();
        btnNovoHorario.setText("Adicionar");
        btnNovoHorario.setMnemonic(KeyEvent.VK_N);
        btnNovoHorario.addActionListener(new IncluirHorarioListener());

        incluirFrame = new HorarioInsereFrame(this);
        editarFrame = new HorarioEditaFrame(this);

        initDB();
    }

    private void adicionaComponentes() {
        add(scrollPane);
        JPanel panel = new JPanel();
        panel.add(btnNovoHorario);
        add(panel, BorderLayout.SOUTH);
    }

    private void initDB() {
        try {
            new HorarioDBDAO().init();
            SwingUtilities.invokeLater(newConsultaHorariosAction());
        } catch (Excecao ex) {
            JOptionPane.showMessageDialog(this, "Falha ao inicializar o Banco de dados: "
                    + ex.getMessage() + "\nVerificar o driver ou configurações do banco", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    public Runnable newConsultaHorariosAction() {
        return new Runnable() {
            public void run() {
                try {
                    HorarioDAO dao = new HorarioDBDAO();
                    tabela.reload(dao.getTodos());
                } catch (Excecao ex) {
                    JOptionPane.showMessageDialog(HorarioFrame.this,
                            ex.getMessage(), "Falha ao consultar horario(s)", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    public void refreshTable(List<Horario> horarios) {
        tabela.reload(horarios);
    }

    private class IncluirHorarioListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            incluirFrame.setVisible(true);
        }
    }

    private class EditarHorarioListener extends MouseAdapter {

        public void mouseClicked(MouseEvent event) {
            if (event.getClickCount() == 2) {
                Horario umHorario = tabela.getHorarioSelecionado();
                if (umHorario != null) {
                    editarFrame.setHorario(umHorario);
                    editarFrame.setVisible(true);
                }
            }
        }
    }

}
