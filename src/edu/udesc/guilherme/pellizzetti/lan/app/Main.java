package edu.udesc.guilherme.pellizzetti.lan.app;

import edu.udesc.guilherme.pellizzetti.lan.ui.PrincipalFrame;

public class Main {

    public static void main(String[] args) {
        new PrincipalFrame();
    }

}
