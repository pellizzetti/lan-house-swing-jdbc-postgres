package edu.udesc.guilherme.pellizzetti.lan.dao.cliente;

import edu.udesc.guilherme.pellizzetti.lan.dao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public class ClienteDBDAO implements ClienteDAO {

    private final static String CRIA_TABELA = "CREATE TABLE IF NOT EXISTS cliente (id_cliente SERIAL NOT NULL, nome_cliente VARCHAR(50) NOT NULL, cpf_cliente VARCHAR(14) NOT NULL, telefone_cliente VARCHAR(14) NOT NULL, PRIMARY KEY(id_cliente))";
    private final static String INSERE_CLIENTE = "INSERT INTO cliente (nome_cliente, cpf_cliente, telefone_cliente) VALUES (?, ?, ?)";
    private final static String EDITA_CLIENTE = "UPDATE cliente SET nome_cliente = ?, cpf_cliente = ?, telefone_cliente = ? WHERE id_cliente = ?";
    private final static String EXCLUI_CLIENTE = "DELETE FROM cliente WHERE id_cliente = ?";
    private final static String BUSCA_TODOS_CLIENTES = "SELECT * FROM cliente";
    private final static String BUSCA_CLIENTE_ID = "SELECT * FROM cliente WHERE id_cliente = ?";

    @Override
    public void init() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = conn.createStatement();
            stmt.executeUpdate(CRIA_TABELA);
            conn.commit();
        } catch (SQLException e) {
            throw new Excecao("Falha ao inicializar o banco de dados:" + CRIA_TABELA, e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public void grava(Cliente cliente) throws Excecao {
        if (cliente == null) {
            throw new Excecao("Informe o cliente para salvar");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            if (cliente.getId() == null) {
                stmt = getStatementInsert(conn, cliente);
            } else {
                stmt = getStatementUpdate(conn, cliente);
            }
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao gravar o cliente", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    private PreparedStatement getStatementInsert(Connection conn, Cliente umCliente) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, INSERE_CLIENTE);
        stmt.setString(1, umCliente.getNome());
        stmt.setString(2, umCliente.getCpf());
        stmt.setString(3, umCliente.getTelefone());
        return stmt;
    }

    private PreparedStatement getStatementUpdate(Connection conn, Cliente umCliente) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, EDITA_CLIENTE);
        stmt.setString(1, umCliente.getNome());
        stmt.setString(2, umCliente.getCpf());
        stmt.setString(3, umCliente.getTelefone());
        stmt.setInt(4, umCliente.getId());
        return stmt;
    }

    @Override
    public void exclui(Cliente umCliente) throws Excecao {
        if (umCliente == null || umCliente.getId() == null) {
            throw new Excecao("Informe a cliente para exclusão");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, EXCLUI_CLIENTE);
            stmt.setInt(1, umCliente.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao excluir o cliente!", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public Cliente achaPorID(Integer id) throws Excecao {
        if (id == null || id.intValue() <= 0) {
            throw new Excecao("Informe o id válido para fazer a busca");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Cliente umCliente = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_CLIENTE_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            if (rs.next()) {
                String nome = rs.getString("nome_cliente");
                String cpf = rs.getString("cpf_cliente");
                String telefone = rs.getString("telefone_cliente");

                umCliente = new Cliente(id, nome, cpf, telefone);
            }
            return umCliente;
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar cliente por id";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    @Override
    public List<Cliente> getTodos() throws Excecao {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_TODOS_CLIENTES);
            rs = stmt.executeQuery();

            return toClientes(rs);
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar todas os clientes";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    private List<Cliente> toClientes(ResultSet rs) throws SQLException {
        List<Cliente> lista = new ArrayList<Cliente>();
        while (rs.next()) {
            int id = rs.getInt("id_cliente");
            String nome = rs.getString("nome_cliente");
            String cpf = rs.getString("cpf_cliente");
            String telefone = rs.getString("telefone_cliente");

            lista.add(new Cliente(id, nome, cpf, telefone));
        }
        return lista;
    }

    private static PreparedStatement criaStatement(Connection conn, String sql) throws SQLException {
        if (conn == null) {
            return null;
        }

        return conn.prepareStatement(sql);
    }

}
