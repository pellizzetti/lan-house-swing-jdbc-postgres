package edu.udesc.guilherme.pellizzetti.lan.dao.cliente;

import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Cliente;

public interface ClienteDAO {

	void grava(Cliente cliente);	
	void exclui(Cliente cliente);	
	List<Cliente> getTodos();
	Cliente achaPorID(Integer id);
	void init();
}
