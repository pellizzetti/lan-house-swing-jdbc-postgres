package edu.udesc.guilherme.pellizzetti.lan.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;

public class Conexao {

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String DATABASE = "lan_house";
    private static final String URL = "localhost:5430/";
    private static final String LINK = "jdbc:postgresql://" + URL + DATABASE;
    private static final String USUARIO = "postgres";
    private static final String SENHA = "databox99182599";

    public static Connection getConexao() throws Excecao {
        Connection conn = null;
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(LINK, USUARIO, SENHA);
            conn.setAutoCommit(false);

            return conn;
        } catch (ClassNotFoundException e) {
            throw new Excecao("Driver não encontrado", e);
        } catch (SQLException e) {
            throw new Excecao("Falha ao obter a conexão", e);
        }
    }

    public static void fecharConexao(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
            throw new Excecao("Falha ao fechar a conexão", e);
        }
    }

    public static void fecharStatement(Connection conn, Statement stmt) {
        try {
            if (conn != null) {
                fecharConexao(conn);
            }
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
            throw new Excecao("Falha ao fechar o statement", e);
        }
    }

    public static void fecharResultSet(Connection conn, Statement stmt, ResultSet rs) {
        try {
            if (conn != null || stmt != null) {
                fecharStatement(conn, stmt);
            }
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            throw new Excecao("Falha ao fechar o resultSet", e);
        }
    }

}
