package edu.udesc.guilherme.pellizzetti.lan.dao.horario;

import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public interface HorarioDAO {

	void grava(Horario horario);	
	void exclui(Horario horario);	
	List<Horario> getTodos();
	Horario achaPorID(Integer id);
	void init();
}
