package edu.udesc.guilherme.pellizzetti.lan.dao.horario;

import edu.udesc.guilherme.pellizzetti.lan.dao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Horario;

public class HorarioDBDAO implements HorarioDAO {

    private final static String CRIA_TABELA = "CREATE TABLE IF NOT EXISTS horario (id_horario SERIAL NOT NULL, cliente_horario INTEGER NOT NULL, equipamento_horario INTEGER NOT NULL, quantidade_hora_horario numeric(4,2) NOT NULL, PRIMARY KEY(id_horario))";
    private final static String INSERE_HORARIO = "INSERT INTO horario (cliente_horario, equipamento_horario, quantidade_hora_horario) VALUES (?, ?, ?)";
    private final static String EDITA_HORARIO = "UPDATE horario SET cliente_horario = ?, equipamento_horario = ?, quantidade_hora_horario = ? WHERE id_horario = ?";
    private final static String EXCLUI_HORARIO = "DELETE FROM horario WHERE id_horario = ?";
    private final static String BUSCA_TODOS_HORARIOS = "SELECT * FROM horario";
    private final static String BUSCA_HORARIO_ID = "SELECT * FROM horario WHERE id_horario = ?";

    @Override
    public void init() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = conn.createStatement();
            stmt.executeUpdate(CRIA_TABELA);
            conn.commit();
        } catch (SQLException e) {
            throw new Excecao("Falha ao inicializar o banco de dados:" + CRIA_TABELA, e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public void grava(Horario horario) throws Excecao {
        if (horario == null) {
            throw new Excecao("Informe o horario para salvar");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            if (horario.getId() == null) {
                stmt = getStatementInsert(conn, horario);
            } else {
                stmt = getStatementUpdate(conn, horario);
            }
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao gravar o horario", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    private PreparedStatement getStatementInsert(Connection conn, Horario umaHorario) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, INSERE_HORARIO);
        stmt.setInt(1, umaHorario.getCliente());
        stmt.setInt(2, umaHorario.getEquipamento());
        stmt.setDouble(3, umaHorario.getQntHoras());        
        return stmt;
    }

    private PreparedStatement getStatementUpdate(Connection conn, Horario umaHorario) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, EDITA_HORARIO);
        stmt.setInt(1, umaHorario.getCliente());
        stmt.setInt(2, umaHorario.getEquipamento());
        stmt.setDouble(3, umaHorario.getQntHoras());
        stmt.setInt(4, umaHorario.getId());
        return stmt;
    }

    @Override
    public void exclui(Horario umaHorario) throws Excecao {
        if (umaHorario == null || umaHorario.getId() == null) {
            throw new Excecao("Informe o horario para exclusão");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, EXCLUI_HORARIO);
            stmt.setInt(1, umaHorario.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao excluir o horario", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public Horario achaPorID(Integer id) throws Excecao {
        if (id == null || id.intValue() <= 0) {
            throw new Excecao("Informe o id válido para fazer a busca");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Horario umaHorario = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_HORARIO_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            if (rs.next()) {
                Integer cliente = rs.getInt("cliente_horario");
                Integer equipamento = rs.getInt("equipamento_horario");
                Double quantidade_hora = rs.getDouble("quantidade_hora_horario");

                umaHorario = new Horario(id, cliente, equipamento, quantidade_hora);
            }
            return umaHorario;
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar horario por id";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    @Override
    public List<Horario> getTodos() throws Excecao {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_TODOS_HORARIOS);
            rs = stmt.executeQuery();

            return toHorarios(rs);
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar todas os horarios";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    private List<Horario> toHorarios(ResultSet rs) throws SQLException {
        List<Horario> lista = new ArrayList<Horario>();
        while (rs.next()) {
            int id = rs.getInt("id_horario");
            Integer cliente = rs.getInt("cliente_horario");
            Integer equipamento = rs.getInt("equipamento_horario");
            Double quantidade_hora = rs.getDouble("quantidade_hora_horario");

            lista.add(new Horario(id, cliente, equipamento, quantidade_hora));
        }
        return lista;
    }

    private static PreparedStatement criaStatement(Connection conn, String sql) throws SQLException {
        if (conn == null) {
            return null;
        }

        return conn.prepareStatement(sql);
    }

}
