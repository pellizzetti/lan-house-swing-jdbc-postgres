package edu.udesc.guilherme.pellizzetti.lan.dao.equipamento;

import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;

public interface EquipamentoDAO {

	void grava(Equipamento equipamento);	
	void exclui(Equipamento equipamento);	
	List<Equipamento> getTodos();
	Equipamento achaPorID(Integer id);
	void init();
}
