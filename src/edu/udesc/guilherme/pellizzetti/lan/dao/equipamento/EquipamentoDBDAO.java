package edu.udesc.guilherme.pellizzetti.lan.dao.equipamento;

import edu.udesc.guilherme.pellizzetti.lan.dao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Equipamento;

public class EquipamentoDBDAO implements EquipamentoDAO {

    private final static String CRIA_TABELA = "CREATE TABLE IF NOT EXISTS equipamento (id_equipamento SERIAL NOT NULL, serial_equipamento VARCHAR(25) NOT NULL, id_categoria integer NOT NULL, valor_equipamento numeric(8,2) NOT NULL, PRIMARY KEY(id_equipamento))";
    private final static String INSERE_EQUIPAMENTO = "INSERT INTO equipamento (serial_equipamento, id_categoria, valor_equipamento) VALUES (?, ?, ?)";
    private final static String EDITA_EQUIPAMENTO = "UPDATE equipamento SET serial_equipamento = ?, id_categoria = ?, valor_equipamento = ? WHERE id_equipamento = ?";
    private final static String EXCLUI_EQUIPAMENTO = "DELETE FROM equipamento WHERE id_equipamento = ?";
    private final static String BUSCA_TODOS_EQUIPAMENTOS = "SELECT * FROM equipamento";
    private final static String BUSCA_EQUIPAMENTO_ID = "SELECT * FROM equipamento WHERE id_equipamento = ?";

    @Override
    public void init() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = conn.createStatement();
            stmt.executeUpdate(CRIA_TABELA);
            conn.commit();
        } catch (SQLException e) {
            throw new Excecao("Falha ao inicializar o banco de dados:" + CRIA_TABELA, e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public void grava(Equipamento equipamento) throws Excecao {
        if (equipamento == null) {
            throw new Excecao("Informe o equipamento para salvar");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            if (equipamento.getId() == null) {
                stmt = getStatementInsert(conn, equipamento);
            } else {
                stmt = getStatementUpdate(conn, equipamento);
            }
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao gravar o equipamento", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    private PreparedStatement getStatementInsert(Connection conn, Equipamento umEquipamento) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, INSERE_EQUIPAMENTO);
        stmt.setString(1, umEquipamento.getSerial());
        stmt.setInt(2, umEquipamento.getCategoria());
        stmt.setDouble(3, umEquipamento.getValor());
        return stmt;
    }

    private PreparedStatement getStatementUpdate(Connection conn, Equipamento umEquipamento) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, EDITA_EQUIPAMENTO);
        stmt.setString(1, umEquipamento.getSerial());
        stmt.setInt(2, umEquipamento.getCategoria());
        stmt.setDouble(3, umEquipamento.getValor());
        stmt.setInt(4, umEquipamento.getId());
        return stmt;
    }

    @Override
    public void exclui(Equipamento umEquipamento) throws Excecao {
        if (umEquipamento == null || umEquipamento.getId() == null) {
            throw new Excecao("Informe a equipamento para exclusão");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, EXCLUI_EQUIPAMENTO);
            stmt.setInt(1, umEquipamento.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao excluir o equipamento!", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public Equipamento achaPorID(Integer id) throws Excecao {
        if (id == null || id.intValue() <= 0) {
            throw new Excecao("Informe o id válido para fazer a busca");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Equipamento umEquipamento = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_EQUIPAMENTO_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            if (rs.next()) {
                String serial = rs.getString("serial_equipamento");
                Integer categoria = rs.getInt("id_categoria");
                //Categoria categoria =  getCategoriaAt(rs.getInt("id_categoria"));
                double valor = rs.getDouble("valor_equipamento");

                umEquipamento = new Equipamento(id, serial, categoria, valor);
            }
            return umEquipamento;
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar equipamento por id";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    @Override
    public List<Equipamento> getTodos() throws Excecao {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_TODOS_EQUIPAMENTOS);
            rs = stmt.executeQuery();

            return toEquipamentos(rs);
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar todas os equipamentos";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    private List<Equipamento> toEquipamentos(ResultSet rs) throws SQLException {
        List<Equipamento> lista = new ArrayList<Equipamento>();
        while (rs.next()) {
            int id = rs.getInt("id_equipamento");
            String serial = rs.getString("serial_equipamento");
            Integer categoria = rs.getInt("id_categoria");
            //Categoria categoria =  getCategoriaAt(rs.getInt("id_categoria"));
            double valor = rs.getDouble("valor_equipamento");

            lista.add(new Equipamento(id, serial, categoria, valor));
        }
        return lista;
    }

    private static PreparedStatement criaStatement(Connection conn, String sql) throws SQLException {
        if (conn == null) {
            return null;
        }

        return conn.prepareStatement(sql);
    }

}
