package edu.udesc.guilherme.pellizzetti.lan.dao.categoria;

import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public interface CategoriaDAO {

	void grava(Categoria categoria);	
	void exclui(Categoria categoria);	
	List<Categoria> getTodos();
	Categoria achaPorID(Integer id);
	void init();
}
