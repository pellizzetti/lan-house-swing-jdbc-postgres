package edu.udesc.guilherme.pellizzetti.lan.dao.categoria;

import edu.udesc.guilherme.pellizzetti.lan.dao.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.udesc.guilherme.pellizzetti.lan.ex.Excecao;
import edu.udesc.guilherme.pellizzetti.lan.modelo.Categoria;

public class CategoriaDBDAO implements CategoriaDAO {

    private final static String CRIA_TABELA = "CREATE TABLE IF NOT EXISTS categoria (id_categoria SERIAL NOT NULL, descricao_categoria VARCHAR(25) NOT NULL, valor_hora_categoria numeric(4,2) NOT NULL, PRIMARY KEY(id_categoria))";
    private final static String INSERE_CATEGORIA = "INSERT INTO categoria (descricao_categoria, valor_hora_categoria) VALUES (?, ?)";
    private final static String EDITA_CATEGORIA = "UPDATE categoria SET descricao_categoria = ?, valor_hora_categoria = ? WHERE id_categoria = ?";
    private final static String EXCLUI_CATEGORIA = "DELETE FROM categoria WHERE id_categoria = ?";
    private final static String BUSCA_TODAS_CATEGORIAS = "SELECT * FROM categoria";
    private final static String BUSCA_CATEGORIA_ID = "SELECT * FROM categoria WHERE id_categoria = ?";

    @Override
    public void init() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = conn.createStatement();
            stmt.executeUpdate(CRIA_TABELA);
            conn.commit();
        } catch (SQLException e) {
            throw new Excecao("Falha ao inicializar o banco de dados:" + CRIA_TABELA, e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public void grava(Categoria categoria) throws Excecao {
        if (categoria == null) {
            throw new Excecao("Informe a categoria para salvar");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            if (categoria.getId() == null) {
                stmt = getStatementInsert(conn, categoria);
            } else {
                stmt = getStatementUpdate(conn, categoria);
            }
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao gravar a categoria", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    private PreparedStatement getStatementInsert(Connection conn, Categoria umaCategoria) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, INSERE_CATEGORIA);
        stmt.setString(1, umaCategoria.getDescricao());
        stmt.setDouble(2, umaCategoria.getValorHora());
        return stmt;
    }

    private PreparedStatement getStatementUpdate(Connection conn, Categoria umaCategoria) throws SQLException {
        PreparedStatement stmt = criaStatement(conn, EDITA_CATEGORIA);
        stmt.setString(1, umaCategoria.getDescricao());
        stmt.setDouble(2, umaCategoria.getValorHora());
        stmt.setInt(3, umaCategoria.getId());
        return stmt;
    }

    @Override
    public void exclui(Categoria umaCategoria) throws Excecao {
        if (umaCategoria == null || umaCategoria.getId() == null) {
            throw new Excecao("Informe a categoria para exclusão");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, EXCLUI_CATEGORIA);
            stmt.setInt(1, umaCategoria.getId());
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (Exception sx) {
            }
            throw new Excecao("Falha ao excluir a categoria!", e);
        } finally {
            Conexao.fecharStatement(conn, stmt);
        }
    }

    @Override
    public Categoria achaPorID(Integer id) throws Excecao {
        if (id == null || id.intValue() <= 0) {
            throw new Excecao("Informe o id válido para fazer a busca");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Categoria umaCategoria = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_CATEGORIA_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            if (rs.next()) {
                String descricao = rs.getString("descricao_categoria");
                Double valor_hora = rs.getDouble("valor_hora_categoria");

                umaCategoria = new Categoria(id, descricao, valor_hora);
            }
            return umaCategoria;
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar categoria por id";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    @Override
    public List<Categoria> getTodos() throws Excecao {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexao.getConexao();
            stmt = criaStatement(conn, BUSCA_TODAS_CATEGORIAS);
            rs = stmt.executeQuery();

            return toCategorias(rs);
        } catch (SQLException e) {
            String errorMsg = "Falha ao consultar todas os categorias";
            throw new Excecao(errorMsg, e);
        } finally {
            Conexao.fecharResultSet(conn, stmt, rs);
        }
    }

    private List<Categoria> toCategorias(ResultSet rs) throws SQLException {
        List<Categoria> lista = new ArrayList<Categoria>();
        while (rs.next()) {
            int id = rs.getInt("id_categoria");
            String descricao = rs.getString("descricao_categoria");
            Double valor_hora = rs.getDouble("valor_hora_categoria");

            lista.add(new Categoria(id, descricao, valor_hora));
        }
        return lista;
    }

    private static PreparedStatement criaStatement(Connection conn, String sql) throws SQLException {
        if (conn == null) {
            return null;
        }

        return conn.prepareStatement(sql);
    }

}
