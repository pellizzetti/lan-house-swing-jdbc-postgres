# README #

Trabalho (Swing + JDBC + Postgres) de Programação 2 - UDESC/CEAVI.

### Lista de afazeres ###

* Desenvolver o relacionamentos entre Tabelas/Objetos, (i.e transformar os campos (ID) em comboBox com o campo descrição/serial/nome da tabela estrangeira).
* Reformular e/ou desenvolver alguns tratamentos de exceções (Principalmente em torno das operações CRUD).
* Desenvolver o resto das funções básicas do pagamento.
* Desenvolver o cálculo em runtime do valor total a pagar.
* Organizar a estrutura de alguns imports e limpar o código em geral.

### SQL ###

```SQL
-- Database: lan_house

-- DROP DATABASE lan_house;

CREATE DATABASE lan_house
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;

-- Table: categoria

-- DROP TABLE categoria;

CREATE TABLE categoria
(
  id_categoria serial NOT NULL,
  descricao_categoria character varying(25) NOT NULL,
  valor_hora_categoria numeric(3,2) NOT NULL,
  CONSTRAINT categoria_pkey PRIMARY KEY (id_categoria)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE categoria
  OWNER TO postgres;

-- Table: cliente

-- DROP TABLE cliente;

CREATE TABLE cliente
(
  id_cliente serial NOT NULL,
  nome_cliente character varying(50) NOT NULL,
  cpf_cliente character varying(14) NOT NULL,
  telefone_cliente character varying(14) NOT NULL,
  CONSTRAINT cliente_pkey PRIMARY KEY (id_cliente)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cliente
  OWNER TO postgres;

-- Table: equipamento

-- DROP TABLE equipamento;

CREATE TABLE equipamento
(
  id_equipamento serial NOT NULL,
  serial_equipamento character varying(25) NOT NULL,
  id_categoria integer NOT NULL,
  valor_equipamento numeric(8,2) NOT NULL,
  CONSTRAINT equipamento_pkey PRIMARY KEY (id_equipamento)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE equipamento
  OWNER TO postgres;

-- Table: horario

-- DROP TABLE horario;

CREATE TABLE horario
(
  id_horario serial NOT NULL,
  cliente_horario integer NOT NULL,
  equipamento_horario integer NOT NULL,
  quantidade_hora_horario numeric(4,2) NOT NULL,
  CONSTRAINT horario_pkey PRIMARY KEY (id_horario)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE horario
  OWNER TO postgres;
```